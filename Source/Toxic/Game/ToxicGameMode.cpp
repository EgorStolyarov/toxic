// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ToxicGameMode.h"
#include "ToxicPlayerController.h"
#include "Character/ToxicCharacter.h"
#include "UObject/ConstructorHelpers.h"

AToxicGameMode::AToxicGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AToxicPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Blueprint'/Game/Blueprint/Character/BP_Character'"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}