// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ToxicPlayerController.generated.h"

class AToxicCharacter;

UCLASS()
class AToxicPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AToxicPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */

	// Begin PlayerController interface
	
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
};


