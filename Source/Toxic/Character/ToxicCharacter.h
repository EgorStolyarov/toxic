// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Weapons/WeaponDefault.h"
#include "ToxicCharacter.generated.h"

UCLASS(Blueprintable)
class AToxicCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AToxicCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

protected:
	virtual void BeginPlay() override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	// Cursor
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.f, 40.f, 40.f);
	UDecalComponent* CurrentCursor = nullptr;	

	// Movement
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	// Camera
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraInit")
	float HeightCameraInitial = 1400.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraInit")
	FRotator RotatorCamera = FRotator(-80.f, 0.f, 0.f);
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraInit")
	bool bFlow = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraInit")
	float CoefSmooth = 5.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraZoom")
	float HeightCameraMin = 300.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraZoom")
	float HeightCameraMax = 3400.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraZoom")
	float ZoomSmoothness = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CameraZoom")
	float ZoomUnits = 500.f;	

	// Weapon
	AWeaponDefault* CurrentWeapon = nullptr;
	/* Demo */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	// Inputs
	UFUNCTION()
	void InputAxisX(float Value);
    UFUNCTION()
    void InputAxisY(float Value);
	UFUNCTION()
    void InputMouseWheel(float Value);
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();	

    float AxisX = 0.f;
    float AxisY = 0.f;
	float AxisWheel = 0.f;
	float DesiredArmLength = HeightCameraInitial;
	FVector SprintDirection;
	
	/* Function used to provide GD a safe access to Cursor Component */
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

    UFUNCTION()
    	void MovementTick(float DeltaTime);	
	UFUNCTION()
    	void ZoomTick(float DeltaTime);
	void CharacterUpdate();
    UFUNCTION(BlueprintCallable)
    	void ChangeMovementState();	
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
    UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION()
		void WeaponReloadStart();
	UFUNCTION()
		void WeaponReloadEnd();
	UFUNCTION()
		void WeaponFire();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFire_BP();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();
};