// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "ToxicCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Engine/World.h"
#include "Game/ToxicGameInstance.h"

AToxicCharacter::AToxicCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = HeightCameraInitial;
	CameraBoom->SetRelativeRotation(RotatorCamera);
	CameraBoom->bEnableCameraLag = bFlow;
	CameraBoom->CameraLagSpeed = CoefSmooth;
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AToxicCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	ZoomTick(DeltaSeconds);
}

void AToxicCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void AToxicCharacter::SetupPlayerInputComponent(class UInputComponent* NewInputComponent)
{
    Super::SetupPlayerInputComponent(NewInputComponent);

    NewInputComponent->BindAxis(TEXT("MoveForward"), this, &AToxicCharacter::InputAxisX);
    NewInputComponent->BindAxis(TEXT("MoveRight"), this, &AToxicCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AToxicCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AToxicCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AToxicCharacter::TryReloadWeapon);
	
	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &AToxicCharacter::InputMouseWheel);
}

void AToxicCharacter::InputAxisX(float Value)
{
    AxisX = Value;
}

void AToxicCharacter::InputAxisY(float Value)
{
    AxisY = Value;
}

void AToxicCharacter::InputMouseWheel(float Value)
{	
	if (Value != 0)
	{
		DesiredArmLength =CameraBoom->TargetArmLength - Value * ZoomUnits;
		if (DesiredArmLength > HeightCameraMax || DesiredArmLength < HeightCameraMin) {
			DesiredArmLength = FMath::Min<float>(FMath::Max<float>(DesiredArmLength, HeightCameraMin), HeightCameraMax);
		}
	}
}

void AToxicCharacter::MovementTick(float DeltaTime)
{
    if (MovementState == EMovementState::SprintRun_State)
	{
		AddMovementInput(SprintDirection);
	}
	else 
	{
		AddMovementInput(FVector(1.0f,0.0f,0.0f), AxisX);
    	AddMovementInput(FVector(0.0f,1.0f,0.0f), AxisY);

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, ResultHit);

			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FRotator(0.0f, FindRotatorResultYaw, 0.0f));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0); // height we add to target point to shoot floor-parallel shots
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				//aim cursor like 3d Widget?
			}
		}
	}	    
}

void AToxicCharacter::ZoomTick(float DeltaTime)
{
	// Only call when need to, saves MS's 
	if (!FMath::IsNearlyEqual(CameraBoom->TargetArmLength, DesiredArmLength, 0.5f)) {
		CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength, DesiredArmLength, DeltaTime, ZoomSmoothness);
	}
}

void AToxicCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void AToxicCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AToxicCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("AToxicCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* AToxicCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AToxicCharacter::InitWeapon(FName IdWeaponName)
{
	UToxicGameInstance* myGI = Cast<UToxicGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					
					myWeapon->WeaponSettings = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

					// DEBUG
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AToxicCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AToxicCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponAttack.AddDynamic(this, &AToxicCharacter::WeaponFire);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AToxicCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void AToxicCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		// если оружие уже не перезаряжается, и если неполная обойма
		if (!CurrentWeapon->WeaponReloading && CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSettings.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void AToxicCharacter::WeaponReloadStart()
{
	WeaponReloadStart_BP();
}

void AToxicCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void AToxicCharacter::WeaponFire() {
	//UE_LOG(LogTemp, Warning, TEXT("AToxicCharacter::WeaponFire - PLAYING ANIM"));
	//UAnimMontage* Anim = CurrentWeapon->WeaponSettings.AnimCharFire;
	//PlayAnimMontage(Anim, 1.0f, FName("Ironsights"));
	WeaponFire_BP();
}

void AToxicCharacter::WeaponReloadStart_BP_Implementation()
{
	// in BP
}

void AToxicCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void AToxicCharacter::WeaponFire_BP_Implementation()
{
	// in BP
}

void AToxicCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AToxicCharacter::ChangeMovementState()
{
	if (SprintRunEnabled)
	{
		WalkEnabled = false;
		AimEnabled = false;
		MovementState = EMovementState::SprintRun_State;
		SprintDirection = GetActorForwardVector();
	}
	else if (WalkEnabled)
	{
		if (AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			MovementState = EMovementState::Walk_State;
		}

	}
	else if (AimEnabled)
	{
		MovementState = EMovementState::Aim_State;
	}
	else
	{
		MovementState = EMovementState::Run_State;
	}
	CharacterUpdate();
	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

UDecalComponent* AToxicCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

